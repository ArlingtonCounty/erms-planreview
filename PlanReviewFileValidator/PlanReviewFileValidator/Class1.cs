﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Aspose.Pdf;
using Aspose.Pdf.Facades;

namespace Public.FileValidator
{
	public class Validator
	{

        // non empty string contains error message
		// empty string indicates file passed
		public string Validate(string filename)
		{           
			string result = string.Empty;
            Aspose.Pdf.License license = new Aspose.Pdf.License();
            license.SetLicense("Aspose.Pdf.lic");

			try
			{                
				// just a test case to demonstrate a failure case
				if (!File.Exists(filename))
				{
					result = "File is not available";
                    return result;
				}
				else
				{
                    if (Path.GetExtension(filename).ToLower() != ".pdf")
                    {
                        result = "The file is not a PDF.";
                        return result;
                    }

                    //Determine if file is corrupt
                    try
                    { 
                        Document doc = new Document(filename);
                    }
                    catch(Exception ex)
                    { 
                        result = "The file appears to be corrupt.  Please confirm it can be opened without error in a PDF Viewer.";
                        return result;
                    }
                    
                    // load the source PDF document
                    PdfFileInfo fileInfo = new PdfFileInfo(filename);
                    
                    // determine that source PDF file is Encrypted with password
                    bool encrypted = fileInfo.IsEncrypted;
                    
                    if (encrypted==true) {
                        result = "The file is password protected or has other security restrictions.";
                        return result;
                    }

                    //Detect AutoCAD SHX Text Comments
                    
                    long lngAnnotations = 0;
                    foreach (Aspose.Pdf.InteractiveFeatures.Annotations.MarkupAnnotation annotation in fileInfo.Document.Pages[1].Annotations)
                    {
                        if (annotation.Title == "AutoCAD SHX Text") {
                            lngAnnotations++;
                        }
                        if (lngAnnotations > 100) {
                            result = "The file includes AutoCAD SHX Text comments. Please refer to the top of this page for more information.";
                            return result;
                        }
                    }

                    /*
                    //Delete AutoCAD SHX Text Comments
                    bool docModified = false;
                    foreach (Aspose.Pdf.InteractiveFeatures.Annotations.MarkupAnnotation annotation in fileInfo.Document.Pages[1].Annotations.OfType<Aspose.Pdf.InteractiveFeatures.Annotations.MarkupAnnotation>())
                    {
                        if (annotation.Title == "AutoCAD SHX Text")
                        {
                            fileInfo.Document.Pages[1].Annotations.Delete(annotation);
                            docModified = true;
                        }
                    }
                    if (docModified == true) {
                        fileInfo.Save(filename);
                    }
                     *                      * */
                }
			}
			catch(Exception ex)
			{
				result = "File access failed";
			}
            return result;
		}
	}
}
